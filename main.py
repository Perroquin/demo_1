import xlsxwriter
from tika import parser
from tika import language
from xml.dom import minidom
from PyPDF2 import PdfFileReader
import spacy

# parser le pdf avec tika pour obtenir du texte
parsed_pdf = parser.from_file("pdf_files/Volvo_2020.pdf", xmlContent=True)
data = parsed_pdf['content']
meta = parsed_pdf['metadata']

# Récupération de la langue et affichage de celle ci
docLang = language.from_buffer(data)
print("Document language:", docLang)

#récupération du nombre de charactère pour chaque page
nbChar = 0
print(len(meta["pdf:charsPerPage"]))
for pageChar in meta['pdf:charsPerPage']:
    nbChar = nbChar + int(pageChar)


nlp = spacy.load("en_core_web_sm")
doc = nlp(data)

nbGPE = 0
nbORG = 0
nbPERSON = 0
for ent in doc.ents:
    if ent.label_ == "GPE":
        nbGPE += 1
    if ent.label_ == "ORG":
        nbORG += 1
    if ent.label_ == "PERSON":
        nbPERSON += 1

print("Nombre de GPE : " + str(nbGPE))
print("Nombre de ORG : " + str(nbORG))
print("Nombre de PERSON : " + str(nbPERSON))

# créer le fichier xml et le remplir avec le résultat de tika
newfile = open("newfile.xml", "w+", encoding="utf-8")
newfile.write(data)

# parser le xml avec minidom pour accéder aux balises
file = minidom.parse('newfile.xml')
metas = file.getElementsByTagName('meta')
print("metas = " + str(len(metas)))
paragraphes = file.getElementsByTagName('p')
print("paragraphes = " + str(len(paragraphes)))

title = ""
subject = ""
author = ""

# parcourir le fichier xml pour récupérer les infos
for i in range(len(metas)):
    if metas[i].attributes['name'].value == "pdf:docinfo:title":
        title = metas[i].attributes['content'].value
        print(title)
    if metas[i].attributes['name'].value == "subject":
        subject = metas[i].attributes['content'].value
        print(subject)
    if metas[i].attributes['name'].value == "meta:author":
        author = metas[i].attributes['content'].value
        print(author)

newfile.close()

# créer et remplir l'excel
workbook = xlsxwriter.Workbook('xml_infos.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, "File Name")
worksheet.write(0, 1, "Number of pages")
worksheet.write(0, 2, "File language")
worksheet.write(0, 3, "File Subject")
worksheet.write(0, 4, "Author")
worksheet.write(0, 5, "Number of paragraph")
worksheet.write(0, 6, "Number of GPE (spacy)")
worksheet.write(0, 7, "Number of ORG (spacy)")
worksheet.write(0, 8, "Number of PERSON (spacy)")
worksheet.write(0, 9, "Number of character in pdf")
worksheet.write(0, 10, "Number of character in each page")

# File name
row = 1
col = 0
worksheet.write(row, col, title)

# Number of pages
col = 1
pdf = PdfFileReader(open('pdf_files/Volvo_2020.pdf', 'rb'))
worksheet.write(row, col, pdf.getNumPages())

# File language
col = 2
worksheet.write(row, col, docLang)

# File subject
col = 3
worksheet.write(row, col, subject)

# Author
col = 4
worksheet.write(row, col, author)

col = 5
worksheet.write(row, col, len(paragraphes))

col = 6
worksheet.write(row, col, nbGPE)

col = 7
worksheet.write(row, col, nbORG)

col = 8
worksheet.write(row, col, nbPERSON)

col = 9
worksheet.write(row, col, nbChar)

col = 10
for chars in meta["pdf:charsPerPage"]:
    worksheet.write(row, col, int(chars))
    row += 1

workbook.close()